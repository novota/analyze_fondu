from openpyxl import *
import pandas as pd
import numpy as np


# goes through the 1st line from A to y of a worksheet and returns the letter of column with column name
def get_column_letter(ws, column_name):
    letter = 65  # 65 is ascii for A
    index = 2  # which row we go through
    go_on = True
    while go_on:
        cell_idx = chr(letter) + str(index)  # creates A1, B1 and so on
        curr_value = ws[cell_idx].value
        if curr_value == column_name:
            return letter
        letter += 1  # try next letter
        if chr(letter) == 'Z':
            go_on = False
    return None


# localizes collumn with column_name and extracts all values until None comes, returns python list of data
def get_data_from_column(ws, column_name):
    column_letter = get_column_letter(ws, column_name)
    result = []
    if column_letter is None:
        print('no column found with given column_name')
        return None
    index = 3
    go_on = True
    while go_on:
        cell_idx = chr(column_letter) + str(index)  # create current cell index eg. A10
        curr_data = ws[cell_idx].value
        if curr_data is None:
            go_on = False
        else:
            result.append(curr_data)
            index += 1
    return result


# return a np.array of all one time investments results with given length
def jednorazova_investice_zisk(ceny, delka):
    if delka > ceny.size:
        print('delka investice je vetsi nez historucka data fondu')
        return None
    start_prizes = ceny[:ceny.size - delka + 1]
    end_prizes = ceny[delka - 1:]
    return (np.power(np.power(end_prizes/start_prizes, 1/delka), 52) - 1) * 100  # np.array zisku v procentech p.a.


def pravidelna_investice_zisk(ceny, krok, delka):
    prav_investice = 10  # vyse pravidelneho vkladu
    start_idx = 0
    end_idx = delka
    zisk = np.zeros(ceny.size-delka + 1)  # za kazde inv. obdobi

    pocet_koupenych_pl = prav_investice / ceny  # for each day

    while end_idx <= ceny.size:
        zisk[start_idx] = (np.power(np.power(pocet_koupenych_pl[start_idx:end_idx:krok].sum() * ceny[end_idx-1]
                           / (((end_idx - start_idx)//krok + 1) * prav_investice), 1/delka), 52) - 1) * 100
        start_idx += 1
        end_idx += 1
    return zisk
# (pocet_koupenych_pl[start_idx:end_idx:krok].sum() * ceny[end_idx-1]
#                            / (((end_idx - start_idx)//krok + 1) * prav_investice) - 1) * 100

# vrati neco jako horni troojuhelnikovou mat. s vysledky vsech jednorazovych investic s postupne se prodluzujicim
# inv. horizontem a vrati vektor investicnich horizontu investic v letech
def vsechny_jednorazy_zisk(ceny, nejkratsi_inv_horizont, krok):
    cols = (ceny.size - nejkratsi_inv_horizont) // krok + 1
    rows = ceny.size - nejkratsi_inv_horizont + 1
    result = np.zeros((rows, cols))

    curr_col_idx = 0
    delka = nejkratsi_inv_horizont
    while curr_col_idx < cols:
        temp = jednorazova_investice_zisk(ceny, delka)
        result[:temp.size, curr_col_idx] = temp
        delka += krok
        curr_col_idx += 1
    return result, np.linspace(nejkratsi_inv_horizont, delka, cols)/52  # returns investment horizonts and zisk


def vsechny_pravidelny_zisk(ceny, nejkratsi_inv_horizont, krok):
    cols = (ceny.size - nejkratsi_inv_horizont) // krok + 1
    rows = ceny.size - nejkratsi_inv_horizont + 1
    result = np.zeros((rows, cols))

    curr_col_idx = 0
    inv_horizont = nejkratsi_inv_horizont
    while curr_col_idx < cols:
        temp = pravidelna_investice_zisk(ceny, 22, inv_horizont)  # ceny, kazdych kolik dni se nakupuje, inv_horizont
        result[:temp.size, curr_col_idx] = temp
        inv_horizont += krok
        curr_col_idx += 1
    return result, np.linspace(nejkratsi_inv_horizont, inv_horizont, cols)/52  # returns investment horizonts and zisk



# vrati, kolik je procent nezapornych hodnot v arr
def pomer(arr):
    return len(arr[arr >= 0]) / (len(arr[arr >= 0]) + len(arr[arr < 0])) * 100












