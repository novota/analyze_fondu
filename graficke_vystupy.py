import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import PercentFormatter

from prace_s_daty import pomer


def vytvor_graf_vystup(matice_vysledku_investic, inv_horizonty):
    shape = matice_vysledku_investic.shape
    cols = shape[1]  # extract row value from touple rows
    maxima = np.apply_along_axis(lambda v: np.max(v[np.nonzero(v)]), 0, matice_vysledku_investic)  # ignore zeros
    minima = np.apply_along_axis(lambda v: np.min(v[np.nonzero(v)]), 0, matice_vysledku_investic)  # ignore zeros
    mediany = np.apply_along_axis(lambda v: np.median(v[np.nonzero(v)]), 0, matice_vysledku_investic)  # ignore zeros
    nezaporna_zhodnoceni = np.apply_along_axis(lambda v: pomer(v[np.nonzero(v)]), 0, matice_vysledku_investic)

    fig, ax1 = plt.subplots()
    ax1.set_xlabel('Investiční horizont')
    ax1.set_ylabel('Zhodnocení p.a.')
    ax1.plot(inv_horizonty, maxima, 'g', inv_horizonty, minima, 'r', inv_horizonty, mediany, 'b')
    ax1.yaxis.set_major_formatter(PercentFormatter())
    ax1.minorticks_on()
    ax1.grid(which='major', linestyle='-', linewidth='0.6')
    ax1.grid(which='minor', linestyle=':', linewidth='0.5')
    plt.legend(['maximální výnost', 'minimální výnos', 'medián'], loc=4)

    ax2 = ax1.twinx()  # second pair of axes which share the x-axis
    ax2.set_ylabel('Podíl investic s nezáporným zhodnocením', color='c')
    ax2.plot(inv_horizonty, nezaporna_zhodnoceni, 'c')
    ax2.yaxis.set_major_formatter(PercentFormatter())
    plt.legend(['podíl nezáporných investic'], loc=0)

    plt.show()
