from openpyxl import *
import numpy as np

from graficke_vystupy import vytvor_graf_vystup
from prace_s_daty import get_data_from_column, jednorazova_investice_zisk, pravidelna_investice_zisk, \
    vsechny_jednorazy_zisk, vsechny_pravidelny_zisk

# data extraction
file_name = '/Users/petrnovota/Documents/Programming/Hodnoceni_fondu/S&P500.xlsx'
wb = load_workbook(file_name)
ws = wb.active
cena = get_data_from_column(ws, 'Cena')
datum = get_data_from_column(ws, 'Datum')

# convert to np.array
cena = np.array(cena)
datumy = np.array(datum)

cena = cena[1150:]
datumy = datumy[1150:]

inv_horizont = 52  # 255 days is ca 1 year of data

jednoraz = jednorazova_investice_zisk(cena, inv_horizont)
prav = pravidelna_investice_zisk(cena, 30, inv_horizont)


print(jednoraz)

vsechny_jednorazy, inv_horizonty_jedn = vsechny_jednorazy_zisk(cena, inv_horizont, 10)
vsechny_pravidelny, inv_horizonty_prav = vsechny_pravidelny_zisk(cena, inv_horizont, 20)
vytvor_graf_vystup(vsechny_pravidelny, inv_horizonty_prav)
# vytvor_graf_vystup(vsechny_jednorazy, inv_horizonty_jedn)





